<?php
namespace App\Models;
use \PDO;
use App\Core\Model; 
use App\Core\Field;
use \App\Validators\NumberValidator;
use \App\Validators\DateTimeValidator;
use \App\Validators\StringValidator;


class ExhibitionModel extends Model {
    protected function getFields(): array{
        return [
            'exhibition_id' => new Field((new NumberValidator())->setIntegerLength(11), false),
            'created_at'    => new Field((new DateTimeValidator())->allowDate()),       
            
            'name'          => new Field((new StringValidator())->setMaxLength(60)),
            'museum_id'     => new Field((new NumberValidator())->setIntegerLength(11)),
            'admin_id'      => new Field((new NumberValidator())->setIntegerLength(11))
        ];
    }
    
    public function getByName(string $name){ 
        return $this->getByFieldName('name', $name);
    }

    public function getAllByMuseumId(int $museumId): array{ 
        return $this->getAllByFieldName('museum_id', $museumId);
        
   }

}