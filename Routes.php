<?php
    return [
        App\Core\Route::get('|^admin/login/?$|', 'Main', 'getLogin'),
        App\Core\Route::post('|^admin/login/?$|', 'Main', 'postLogin'),
        App\Core\Route::get('|^admin/logout/?$|', 'Main', 'getLogout'),
        App\Core\Route::get('|^contact/store_locator/?$|', 'Contact', 'storeLocator'),

        //Admin role routes

        App\Core\Route::get('|^admin/profile/?$|', 'AdminDashboard', 'index'),            //ruta za sadrzaj vidljiv samo Adminu

        //Admin role routes za Muzej

        App\Core\Route::get('|^admin/museums/?$|', 'AdminMuseumManagement', 'museums'), // ruta za izmenu muzeja
        App\Core\Route::get('|^admin/museums/edit/([0-9]+)/?$|', 'AdminMuseumManagement', 'getEdit'), // ruta za izmenu muzeja
        App\Core\Route::post('|^admin/museums/edit/([0-9]+)/?$|', 'AdminMuseumManagement', 'postEdit'),
        App\Core\Route::get('|^admin/museums/add/?$|', 'AdminMuseumManagement', 'getAdd'),
        App\Core\Route::post('|^admin/museums/add/?$|', 'AdminMuseumManagement', 'postAdd'),
        App\Core\Route::get('|^admin/reservation/([0-9]+)/?$|', 'Reservation', 'show'),                //ruta za prikazivanje potrebnih informacija o rezervaciji
        App\Core\Route::get('|^admin/reservations/?$|', 'Reservation', 'reservations'),                //ruta za prikazivanje svih rezervacija

        //User reservation

        App\Core\Route::get('|^user/reservation/?$|', 'Main', 'getReservation'),
        App\Core\Route::post('|^user/reservation/?$|', 'Main', 'postReservation'),
        App\Core\Route::post('|^user/reservationTermUser/?$|', 'Main', 'postReservationTermUser'),

        App\Core\Route::get('|^exhibition/([0-9]+)/?$|', 'Exhibition', 'show'),                 //ruta za prikazivanje pojedinacnih izlozbi po id-ju
        App\Core\Route::get('|^exhibitions/?$|', 'Exhibition', 'exhibitions'),          //ruta za prikazivanje svih izlozbi
        App\Core\Route::get('|^museum/([0-9]+)/?$|', 'Museum', 'show'),                     //ruta za prikazivanje pojedinacnih muzeja po id-ju
        App\Core\Route::get('|^term/([0-9]+)/?$|', 'Term', 'show'),                       //ruta za prikazivanje svih termina
        App\Core\Route::get('|^museums/?$|', 'Museum', 'museums'),                  //ruta za prikazivanje svih muzeja
        App\Core\Route::get('|^user/([0-9]+)/?$|', 'User', 'show'),
        App\Core\Route::post('|^search/?$|', 'Term', 'postSearch'),

        //API rute

        App\Core\Route::get('|^api/exhibition/([0-9]+)/?$|', 'ApiExhibition', 'show'),
        App\Core\Route::get('|^api/bookmarks/?$|', 'ApiBookmark', 'getBookmarks'),
        App\Core\Route::get('|^api/bookmarks/add/([0-9]+)/?$|', 'ApiBookmark', 'addBookmark'),
        App\Core\Route::get('|^api/bookmarks/clear/?$|', 'ApiBookmark', 'clear'),

        App\Core\Route::any('|^.*$|', 'Main', 'home'),                    //default-na ruta
];
