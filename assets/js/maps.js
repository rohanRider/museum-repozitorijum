function initMap() {
    0
        var myMapCenter = {lat: 44.782999, lng: 20.478554};
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myMapCenter,
            zoom: 5,
        });
    
    
    
    var stores = [
        {
            name: 'Prirodnjacki centar Svilajnac',
            location: {lat: 44.231862, lng: 21.1924050000000084},
            hours: '8AM to 21PM',
            address: 'Kralja Petra Prvog 111, 35210 Svilajnac',
            tel: '035/8814001',
            fax: '035/8814001'
            
            
            
        },
        {
            name: 'Muzej vostanih figura jagodina',
            location: {lat: 43.9676549, lng: 21.26526960000001},
            hours: '8AM to 21M',
            address: 'Vuka Bojovica 2, 35000 Jagodina',
            tel: '035/252983',
            fax: '035/252983'
            
        },
        
        {
            name: 'Muzej umetnosti',
            location: {lat: 44.782748, lng: 20.478526},
            hours: '10AM to 9PM',
            address: 'Danijelova 32, 11000 BEOGRAD',
            tel: '011 3094 094',
            fax: '+11 2094 095'
            
        },
        {
            name: 'Muzej naive i marginalne umetnosti',
            location: {lat: 43.9777294, lng: 21.257271899999978},
            hours: '10AM to 9PM',
            address: 'Boska Djuricica , 35000 Jagodina',
            tel: '035222222',
            fax: '035222222'
            
        }

    ];
    
    function markStore(storeInfo){
    
        // Create a marker and set its position.
        var marker = new google.maps.Marker({
            map: map,
            position: storeInfo.location,
            title: storeInfo.name
        });
    
        // show store info when marker is clicked
        marker.addListener('click', function(){
            showStoreInfo(storeInfo);
        });
    }
    
    // show store info in text box
    function showStoreInfo(storeInfo){
        var info_div = document.getElementById('info_div');
        info_div.innerHTML = 'Ime: '
            + storeInfo.name
            + '<br>Radno vreme: ' + storeInfo.hours 
            + '<br>Adresa: ' +storeInfo.address 
            + '<br>Tel: ' +storeInfo.tel 
            + '<br>Fax: ' +storeInfo.fax
    
    
    
    
    };
    
        stores.forEach(function(store){
        markStore(store);
    });
    
    }