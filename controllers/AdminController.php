<?php
    namespace App\Controllers;
    use App\Core\DatabaseConnection;
   

    class AdminController extends \App\Core\Controller {
        
        public function show($id){ //prikazivanje admina
            $adminModel = new \App\Models\AdminModel($this->getDatabaseConnection());
            $admin = $adminModel->getById($id);
            $this->set('admin', $admin);
         //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta

        }

        }