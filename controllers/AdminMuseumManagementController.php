<?php
    namespace App\Controllers;
        class AdminMuseumManagementController extends \App\Core\Role\AdminRoleController {
            public function museums() {
            $museumModel = new \App\Models\museumModel($this->getDatabaseConnection());
            $museums = $museumModel->getAll();
            $this->set('museums', $museums);
        }
    
            public function getEdit($museumId){
            $museumModel = new \App\Models\museumModel($this->getDatabaseConnection());
            $museum = $museumModel->getById($museumId);
            
            if(!$museum){
                $this->redirect(\Configuration::BASE . 'admin/museums');
            }
        
            $this->set('museum', $museum);

            return $museumModel;
        
        }

            public function postEdit($museumId){
            $museumModel = $this->getEdit($museumId);
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $museumModel->editById($museumId, [
                'name' => $name
            ]);
            $this->redirect(\Configuration::BASE . 'admin/museums');

        }

        public function getAdd(){

        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $museumModel = new \App\Models\museumModel($this->getDatabaseConnection());
            $museumId = $museumModel->add([
                'name'=> $name,
                
            ]);

            if($museumId){
                $this->redirect(\Configuration::BASE . 'admin/museums');
            }

            $this->set('message', "Doslo je do greske: Nije moguce dodati ovaj muzej");
        }
      
      
    }