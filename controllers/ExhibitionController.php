<?php

namespace App\Controllers;

class ExhibitionController extends \App\Core\Controller
{
    public function show($id)
    { // uzimanje i prikazivanje izlozbi po id-u, 'show metod
        $exhibitionModel = new \App\Models\ExhibitionModel($this->getDatabaseConnection());
        $exhibition = $exhibitionModel->getById($id);

        if (!$exhibition) {
            header('location:/muzej/');
            exit;
        }

        $this->set('exhibition', $exhibition);
        //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta

        $termModel = new \App\Models\TermModel($this->getDatabaseConnection());  // uzimanje id izlozbe i prikazivanje termina u Izlozbi
        $termInExhibition = $termModel->getTermsByExhibition($id);
        $this->set('termInExhibition', $termInExhibition);

        $imageModel = new \App\Models\ImageModel($this->getDatabaseConnection());  // uzimanje id izlozbe i prikazivanje slika u Izlozbi
        $imageInExhibition = $imageModel->getAllByExhibitionId($id);
        $this->set('imageInExhibition', $imageInExhibition);

        //$termInExhibitions = $termModel ->getTermsByExhibition($id);

        $exhibitionViewModel = new \App\Models\ExhibitionViewModel($this->getDatabaseConnection());

        $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');

        $exhibitionViewModel->add(
            [
            'exhibition_id' => $id,
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            ]
         );

        $imageModel = new \App\Models\ImageModel($this->getDatabaseConnection());
        $image = $imageModel->getById($id);
        $this->set('image', $image);
    }

    public function exhibitions()
    { // uzimanje svih podataka iz baze i prikazivanje izlozbi 'exhibitions' metod
        $exhibitionsModel = new \App\Models\ExhibitionModel($this->getDatabaseConnection());
        $exhibitions = $exhibitionsModel->getAll();

        if (!$exhibitions) {
            header('location:/muzej/');
            exit;
        }

        $this->set('exhibitions', $exhibitions);
        //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta
    }
}
