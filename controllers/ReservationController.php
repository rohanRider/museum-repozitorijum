<?php

namespace App\Controllers;

class ReservationController extends \App\Core\Role\AdminRoleController
{
    public function show($id)
    { //prikazivanje rezervacija po id-u
        $reservationModel = new \App\Models\ReservationModel($this->getDatabaseConnection());
        $reservation = $reservationModel->getAllByFieldName('term_id', $id);

        if (!$reservation) {
            header('location: /muzej/');
            exit;
        }

        $this->set('reservation', $reservation);
        //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta

        //    $userModel = new \App\Models\UserModel($this->getDatabaseConnection()); // prikazivanje korisnika za odredjenu rezervaciju
        //    $user = $userModel->getById($reservation->user_id);
        //    $this->set('user', $user);

        //    $languageModel = new \App\Models\LanguageModel($this->getDatabaseConnection()); // prikazivanje korisnika za odredjenu rezervaciju
        //    $language = $languageModel->getById($reservation->language_id);
        //     $this->set('language', $language);

        $reservationModel = new \App\Models\ReservationModel($this->getDatabaseConnection());  // uzimanje id izlozbe i prikazivanje termina u Izlozbi
        $usersInReservation = $reservationModel->getAllUserByTermId($id);
        $this->set('usersInReservation', $usersInReservation);

        $freeSpaceTerm = $reservationModel->getTermsById($id); // prikaz slobodnih mesta za izlozbu

        $this->set('freeSpaceTerm', $freeSpaceTerm[0]->free_space);
    }

    public function reservations()
    { //prikazivanje izlozbi
        $reservationsModel = new \App\Models\ReservationModel($this->getDatabaseConnection());
        $reservations = $reservationsModel->getAll();

        if (!$reservations) {
            header('location:/muzej/');
            exit;
        }

        $this->set('reservations', $reservations);

        $termModel = new \App\Models\TermModel($this->getDatabaseConnection());  // prikazivanje termina
        $terms = $termModel->getAll();
        $this->set('terms', $terms);

        //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta
    }
}
