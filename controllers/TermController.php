<?php

namespace App\Controllers;

class TermController extends \App\Core\Controller
{
    public function show($id)
    { //prikazivanje termina po id-u
        $termModel = new \App\Models\TermModel($this->getDatabaseConnection());
        $term = $termModel->getById($id);

        if (!$term) {
            header('location:/muzej/');
            exit;
        }

        $this->set('term', $term);
        //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta

        $reservationModel = new \App\Models\ReservationModel($this->getDatabaseConnection());  //  prikazivanje rezervacija za odredjeni termin
        $reservationInTerm = $ReservationModel->getAllByTermId($id);
        $this->set('reservationInTerm', $reservationInTerm);
    }

    public function postSearch()
    {
        $termModel = new \App\Models\TermModel($this->getDatabaseConnection());

        $dateTime = \filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

        $dateTime1 = $dateTime.' 00:00';
        $dateTime2 = $dateTime.' 23:59';

        $terms = $termModel->getAllBySearch($dateTime1, $dateTime2);

        $this->set('terms', $terms);
    }
}
