<?php
    namespace App\Controllers;
    use App\Core\DatabaseConnection;
   

    class MuseumController extends \App\Core\Controller {
        
         public function show($id){ //prikazivanje muzeja po id-u
             $museumModel = new \App\Models\MuseumModel($this->getDatabaseConnection());
             $museum = $museumModel->getById($id);
        
             if(!$museum){
                header('location:/muzej/');
                exit;
            } 

            $this->set('museum', $museum);
         //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta
    
            $exhibitionModel = new \App\Models\ExhibitionModel($this->getDatabaseConnection());  //prikazivanje izlozbi za odredjen muzej
            $exhibitionInMuseum = $exhibitionModel ->getAllByMuseumId($id);
            $this->set('exhibitionInMuseum', $exhibitionInMuseum);

         }
         
         public function museums(){ // prikazivanje muzeja
            $museumModel = new \App\Models\MuseumModel($this->getDatabaseConnection());
            $museums = $museumModel->getAll();
           
            if(!$museums){
               header('location:/muzej/');
               exit;
            } 
           
            $this->set('museums', $museums);
            //ne sme echo i generisanje html koda unutar kontrolera. Mora view komponenta
            
            }
        }